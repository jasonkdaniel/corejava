package com.main;

import java.io.IOException;

import com.appexception.UserDefinedException;
import com.service.HRService;

public class UserDefinedExceptionDemo {

	public static void main(String[] args) {
		HRService hrService = new HRService();
		boolean valid;
		try {
			valid = hrService.validateAge(24);
			System.out.println("Eligible");
			boolean returnData = hrService.validateName("ab");

		} catch (UserDefinedException ude) {
			System.err.println(ude.getMessage());
		} // hardcoding

		System.out.println("End of program");
	}

}
