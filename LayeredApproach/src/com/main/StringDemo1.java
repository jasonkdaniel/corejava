package com.main;

import javax.swing.plaf.synth.SynthSplitPaneUI;

public class StringDemo1 {

	public static void main(String[] args) {
		String var = "Happy"; // String literal  == String pool 
		System.out.println(var.length());
		char[ ] charArray= var.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			System.out.println(charArray[i]);
		}
		System.out.println(var.toUpperCase());
		System.out.println(var.toLowerCase());
		
		// String is an object 
		// Employee emp = new Employee()
		// emp =  null;
		// String object 
		String str1 = new String("Happy");
		
		str1= null;
		// solution 
		 // 1) try catch ()
		// how to avoid null pointer
		if(str1 != null && str1.length()>0)
		{
			System.out.println(str1.toLowerCase());
		}
		else
		{
			System.out.println("No data ");
		}
	}

}
