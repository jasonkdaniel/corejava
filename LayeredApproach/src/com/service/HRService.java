package com.service;

import java.io.IOException;

import com.appexception.UserDefinedException;

public class HRService {

	public boolean validateAge(int age) throws UserDefinedException {
		if (age >= 18 && age <= 55) {
			return true;
		} else {
			//raise an exception 
			throw new UserDefinedException("customize the error message");   // custom exception 
		}
	}
	
	public boolean validateName(String name) throws UserDefinedException {
		
			if (name.length() > 3) {
				return true;
			} else {
				throw new UserDefinedException("Check the name");
			}
	}
}
